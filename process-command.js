
// Import the Commands from the Commands Folder

const whoCommand = require('./commands/who.js')
const helpCommand = require('./commands/help.js')
const RNGCommand = require('./commands/rng.js')
const SayCommand = require('./commands/say.js')
const PurgeCommand = require('./commands/purge.js')

// Process the Received Message and execute the desired command.

function processCommand(receivedMessage) {
    let fullCommand = receivedMessage.content.substr(1) 
    let splitCommand = fullCommand.split(" ") 
    let primaryCommand = splitCommand[0] 
    let arguments = splitCommand.slice(1) 

    console.log("Command received: " + primaryCommand)
    console.log("Arguments: " + arguments)

      if (primaryCommand == "help") {
        //receivedMessage.react("❤")
        helpCommand(arguments, receivedMessage)
        receivedMessage.delete(receivedMessage)
      }

      else if (primaryCommand == "who") {
        whoCommand(arguments, receivedMessage)
        receivedMessage.delete(receivedMessage)
      } 

      else if (primaryCommand == "roll") {
        RNGCommand(arguments, receivedMessage)
        receivedMessage.delete(receivedMessage)
      }

      else if (primaryCommand == "say") {
        SayCommand(arguments, receivedMessage)
        receivedMessage.delete(receivedMessage)
      }

      else if (primaryCommand == "purge") {
        PurgeCommand(arguments, receivedMessage)
        receivedMessage.delete(receivedMessage)
      }

      else {
        receivedMessage.delete(receivedMessage)
        receivedMessage.channel.send("I don't understand the command. Try `&help`")
    }
}










module.exports = processCommand