function SayCommand(arguments, receivedMessage) {

    var EntireMessage = ""

    ListOfArguments = Array.from(arguments);
    EntireMessage += ListOfArguments.join(" ")
    receivedMessage.channel.send(EntireMessage)
}

module.exports = SayCommand