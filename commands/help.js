function helpCommand(arguments, receivedMessage) {

    if(arguments == "") {
        receivedMessage.channel.send("***Hiya~!***\nYou were confused on the Commands?"
                                   + "\nThis is the Full Command List, I hope it helps you:"
                                   + "\n"
                                   + "\n**`&who`** - I'll introdouce Myself~!"
                                   + "\n**`&help`** - Helps you see what commands you can use."
                                   + "\n**`&roll`** - Gives you a Random Number Between 1 and 100."
                                   + "\n**`&say`** - I'll say what you want. Please be gentle~")
    }
}

module.exports = helpCommand