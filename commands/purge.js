function PurgeCommand(arguments, receivedMessage) { 
 

    const deleteCount = parseInt(arguments);

    const fetched = receivedMessage.channel.fetchMessages({limit: deleteCount});
    ToDelete = Array.from(fetched);


    receivedMessage.channel.bulkDelete(ToDelete)
      .catch(error => receivedMessage.reply(`Couldn't delete messages because of: ${error}`));

     receivedMessage.channel.send("Purge Command executed")
  }


module.exports = PurgeCommand